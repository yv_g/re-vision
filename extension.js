const vscode = require('vscode');
const languageClient = require('vscode-languageclient/node');
const path = require('path');

let client;

function activate(context) {

	const serverModule = context.asAbsolutePath(path.join('server', 'out', 'server.js'));
  
    const serverOptions = {
        run: { 
            module: serverModule, 
            transport: languageClient.TransportKind.ipc },
        debug: {
            module: serverModule,
            transport: languageClient.TransportKind.ipc,
        }
    };

    const clientOptions = {
        
        documentSelector: [{ scheme: 'file', language: 'clojure' }],
        synchronize: {
            fileEvents: vscode.workspace.createFileSystemWatcher('**/.clientrc')
        }
    };
    
    client = new languageClient.LanguageClient('Re-VisionLanguageServer', 'Re-Vision Language Server', serverOptions, clientOptions);

	let disposable = vscode.commands.registerCommand('re-vision.startRevision', function () {
		vscode.window.showInformationMessage('Re-Vision is running!');
        client.start();
	});

	context.subscriptions.push(disposable);
}

function deactivate() {}

module.exports = {
	activate,
	deactivate
}
