# Re-Vision

Visualisierung von Re-Frame App-state Veränderungen

## Funktionalität

- beeinhaltet die Visual Studio Code-Erweiterung und den Language Server
- beim Hover über ID eines Event-Handlers der Sorte reg-event-db werden potenzielle Veränderungen im App-state, die
sich aus dem Code heraus ergeben, visualisiert


## Ausführung

- in diesem Ordner `npm install` ausführen - dies installiert die erforderlichen npm-Module sowohl im Client- als auch im Server Ordner und transpiliert den ClojureScript-Code zu Javascript
- diesen Ordner in VS Code öffnen
- F5 drücken - dadurch wird die Erweiterung kompiliert und in einem neuen Fenster des Extension Development Host ausgeführt
- den Befehl "Start Re-Vision" aus dem Befehlspalette (Ctrl+Shift+P) im neuen Fenster ausführen
