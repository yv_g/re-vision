const lang_server = require("vscode-languageserver/node");
const text_document = require("vscode-languageserver-textdocument");
const connection = lang_server.createConnection(lang_server.ProposedFeatures.all);
const documents = new lang_server.TextDocuments(text_document.TextDocument);
const revision = require('./revision-lib');
const revisionLib = require("./revision-lib");

let handlerIds = [];
connection.onInitialize(() => {

    return {
		capabilities: {
			hoverProvider: true,
		}
	};
});


documents.onDidChangeContent(change => {
    let file = change.document;
    console.log(revision.saveNamespace(file.getText()))
    handlerIds = revision.ids(file.getText()); 
});

documents.onDidOpen(change => {
  let file = change.document;
  console.log(revision.saveNamespace(file.getText()));
  handlerIds = revision.ids(file.getText());
  revisionLib.test(file.getText());
});



connection.onHover(({ textDocument, position }) => {

  const document = documents.get(textDocument.uri)

    const start = {
      line: position.line,
      character: 0
    }
    const end = {
      line: position.line + 1,
      character: 0
    }
    const text = document.getText({ start, end })

    const index = document.offsetAt(position) - document.offsetAt(start)

    const word = getWord(text, index);  

   if (handlerIds.includes(word.trim())) {

    let representation = revision.showHover(document.getText(), word.trim());
 
    return {
      contents: {
        kind: 'markdown',
        value: representation,
      },
    };
  } else {

    return undefined;
  }
  });

  function getWord(text, index) {
    const first = text.lastIndexOf(" ", index);
    const last = text.indexOf(" ", index);
    return text.substring(
      first !== -1 ? first : 0,
      last !== -1 ? last : text.length 
    )
  };



documents.listen(connection);
connection.listen();

