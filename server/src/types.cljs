(ns src.types)

(def seqable->seq {:ret :seq})

(def seqable->boolean {:ret :boolean})

(def seqable->any {:ret :any})

(def any->boolean {:ret :boolean})

(def a->a {:ret :unknown})

(def number->number {:ret :number})

(def number->number->number {:ret :number})

(def number*->number {:ret :number})

(def number+->number {:ret :number})

(def number->boolean {:ret :boolean})

(def compare-numbers {:ret :boolean})

(def int->int {:ret :int})

(def int->int->int {:ret :int})

(def clojure-core
  {'if {:ret :unknown}
   'list {:ret :list}
   'cons {:ret :unknown}
   'first seqable->any
   'next seqable->seq
   'rest seqable->seq
   'conj {:ret :coll}
   'second seqable->any
   'ffirst seqable->any
   'nfirst seqable->seq
   'fnext seqable->any
   'nnext seqable->seq
   'seq {:ret :seq}
   'instance? any->boolean
   'seq? any->boolean
   'char? any->boolean
   'string? any->boolean
   'map? any->boolean
   'assoc {:ret :unknown}
   'meta {:ret :nilable/map}
   'with-meta a->a
   'last seqable->any
   'butlast seqable->seq
   'vector {:ret :vector}
   'vec {:ret :vector}
   'hash-map {:ret :map}
   'hash-set {:ret :set}
   'sorted-map {:ret :sorted-map}
   'nil? any->boolean
   'when {:ret :unknown}
   'when-not {:ret :unknown}
   'false? any->boolean
   'true? any->boolean
   'boolean? any->boolean
   'not any->boolean
   'some? any->boolean
   'any? any->boolean
   'str {:ret :string}
   'symbol? any->boolean
   'keyword? any->boolean
   'cond {:ret :unknown}
   'symbol {:ret :symbol}
   'keyword {:ret :keyword}
   'list* {:ret :list}
   'apply {:ret :any}
   'lazy-seq {:ret :seq}
   'chunked-seq? any->boolean
   'concat {:ret :seq}
   'delay? any->boolean
   'identical? {:ret :boolean}
   '= {:ret :boolean}
   'not= {:ret :boolean}
   'compare {:ret :number}
   'and {:ret :unknown}
   'or {:ret :unknown}
   'zero? number->boolean
   'count {:ret :number}
   'int {:ret :int}
   'nth {:ret :any}
   '< compare-numbers
   'inc' number->number
   'inc number->number
   'reverse {:ret :seq}
   '+' number*->number
   '+ number*->number
   '*' number*->number
   '* number*->number
   '/ number+->number
   '-' number+->number
   '- number+->number
   '<= compare-numbers
   '> compare-numbers
   '>= compare-numbers
   '== compare-numbers
   'max number+->number
   'min number+->number
   'dec' number->number
   'dec number->number
   'unchecked-inc-int int->int
   'unchecked-inc number->number
   'unchecked-dec-int int->int
   'unchecked-dec number->number
   'unchecked-negate-int int->int
   'unchecked-negate number->number
   'unchecked-add-int int->int->int
   'unchecked-add number->number->number
   'unchecked-subtract-int int->int->int
   'unchecked-subtract number->number->number
   'unchecked-multiply-int int->int->int
   'unchecked-multiply number->number->number
   'unchecked-divide-int int->int->int
   'unchecked-remainder-int int->int->int
   'pos? number->boolean
   'neg? number->boolean
   'quot number->number->number
   'rem number->number->number
   'rationalize number->number
   'integer? any->boolean
   'even? number->boolean
   'odd? number->boolean
   'int? any->boolean
   'pos-int? any->boolean
   'neg-int? any->boolean
   'nat-int? any->boolean
   'double? any->boolean
   'complement {:ret :fn}
   'constantly {:ret :fn}
   'identity a->a
   'peek {:ret :any}
   'pop {:ret :stack}
   'map-entry? any->boolean
   'contains? {:ret :boolean}
   'dissoc {:ret :nilable/map}
   'find {:ret :unknown}
   'select-keys {:ret :map}
   'keys {:ret :seq}
   'vals {:req :seq}
   'rseq {:req :seq}
   'namespace {:ret :string}
   'boolean any->boolean
   'ident? any->boolean
   'simple-ident? any->boolean
   'qualified-ident? any->boolean
   'simple-symbol? any->boolean
   'qualified-symbol? any->boolean
   'simple-keyword? any->boolean
   'qualified-keyword? any->boolean
   'if-let {:ret :unknown}
   'when-let {:ret :unknown}
   'atom {:ret :atom}
   'swap! {:ret :any}
   'reset! {:ret :any}
   'volatile? any->boolean
   'comp {:ret :ifn}
   'juxt {:ret :ifn}
   'partial {:ret :ifn}
   'every? {:ret :boolean}
   'not-every? {:ret :boolean}
   'some {:ret :any}
   'not-any? {:ret :boolean}
   'map {:ret :seq}
   'mapcat {:ret :seq}
   'filter {:ret :seq}
   'remove {:ret :seq}
   'reduced? any->boolean 
   'take {:ret :seq}
   'take-while {:ret :seq}
   'drop {:ret :seq}
   'drop-last {:ret :seq} 
   'take-last {:ret :seq} 
   'drop-while {:ret :seq} 
   'cycle seqable->seq 
   'split-at {:ret :vector} 
   'split-with {:ret :vector}
   'repeat {:ret :seq} 
   'iterate {:ret :seq} 
   'range {:ret :seq}
   'merge {:ret :nilable/map}
   'merge-with {:ret :nilable/map} 
   'zipmap {:ret :map} 
   'partition {:ret :seq}
   'byte {:ret :byte}
   'number? any->boolean
   'ratio? any->boolean
   'decimal? any->boolean
   'float? any->boolean
   'rational? any->boolean
   'subvec {:ret :vector}
   'set? any->boolean
   'set {:ret :set}
   'take-nth {:ret :seq} 
   'fn {:ret :fn} 
   'for {:ret :seq}
   'ex-info {:ret :throwable}
   're-pattern {:ret :regex}
   're-seq {:ret :seq} 
   're-matches {:ret :unknown} 
   're-find {:ret :unknown}
   'tree-seq {:ret :seq}
   'special-symbol? any->boolean 
   'var? any->boolean
   'subs {:ret :string}
   'max-key {:ret :any}
   'min-key {:ret :any}
   'distinct {:ret :seq}
   'interpose {:ret :set}
   'bytes? any->boolean 
   'class? any->boolean
   'format {:ret :string}
   'get-in {:ret :any}
   'assoc-in {:ret :map} 
   'update-in {:ret :map} 
   'update {:ret :map}
   'empty? seqable->boolean 
   'coll? any->boolean 
   'list? any->boolean 
   'seqable? any->boolean 
   'ifn? any->boolean 
   'fn? any->boolean
   'associative? any->boolean 
   'sequential? any->boolean 
   'sorted? any->boolean 
   'counted? any->boolean 
   'reversible? any->boolean 
   'indexed? any->boolean
   'future? any->boolean
   'fnil {:ret :ifn}
   'inst? any->boolean
   'uuid? any->boolean
   'reduce {:ret :any}
   'into {:ret :coll} 
   'mapv {:ret :vector}
   'filterv {:ret :vector}
   'flatten {:ret :sequential} 
   'group-by {:ret :map} 
   'partition-by {:ret :seq}
   'frequencies {:ret :map}
   'partition-all {:ret :seq}
   'shuffle {:ret :coll} 
   'map-indexed {:ret :seq}
   'keep {:ret :seq}
   'keep-indexed {:ret :seq}
   'every-pred {:ret :ifn} 
   'some-fn {:ret :ifn}
   'dedupe {:ret :seq} 
   'tagged-literal? any->boolean
   'reader-conditional? any->boolean
   'uri? any->boolean 
   'throw {:ret :unknown}})

(defn key-to-type [key-type]
  (cond
    (= :unknown key-type) 'unknown?
    (= :boolean key-type) 'boolean?
    (= :string key-type) 'string?
    (= :vector key-type) 'vector?
    (= :list key-type) 'list?
    (= :coll key-type) 'coll?
    (= :keyword key-type) 'keyword?
    (= :seq key-type) 'seq?
    (= :int key-type) 'int?
    (= :map key-type) 'map?
    (= :any key-type) 'any?
    (= :sequential key-type) 'sequential?
    (= :regex key-type) 'regex?
    (= :ifn  key-type) 'ifn?
    (= :byte  key-type) 'byte?
    (= :stack key-type) 'stack?
    (= :list key-type) 'list?
    (= :number key-type) 'number?
    (= :any key-type) 'any?
    (= :atom key-type) 'atom?
    (= :throwable key-type) 'throwable?
    (= :sorted-map key-type) 'sorted-map?
    (= :symbol key-type) 'symbol?))

