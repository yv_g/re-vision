(ns src.handler-parser
  (:require [src.db :refer [get-in-db]] 
            [src.util-parser :refer [parse-re z-next get-current-ns require-as?]]
            [rewrite-clj.zip :as z]
            [clojure.string :as str]))

(defn parse-handler-type [parsed-handler current-ns]
  (let [handler-node (z-next parsed-handler 2)
        require-as? (require-as? (get-in-db [current-ns :requires]) 're-frame.core)]
    (if require-as?
      (second (str/split (z/string handler-node) (re-pattern (str require-as? "/"))))
      (z/string handler-node))))

(defn first-list [handler-node]
  (-> handler-node
      (z/next)
      (z/down)
      (z/right)
      (z/right)
      (z/down)
      (z/right)
      (z/right)))

(defn second-list [list-node] 
  (-> list-node
      (z/down)
      (z/next)
      (z/right)))

(defn let? [handler-node]
  (let [list-node (first-list handler-node)
        fnc (z/sexpr (z-next list-node 1))
        bindings (if (= fnc 'let) 
              (z/string (z-next list-node 2))
              [])]
    {:let? (= fnc 'let) 
     :bindings bindings}))

(defn analyze-bindings [bindings-vec-str]
  (let [parsed (z/down (z-next (parse-re bindings-vec-str) 1))] 
    (loop [{:as state :keys [parsed]} {:parsed parsed
                                       :result {}}] 
       (if (not (z/end? parsed))
         (recur
          (-> state
              (assoc-in [:result (z/sexpr parsed)] (z/sexpr (z/right parsed)))
              (assoc :parsed (z/right (z/right parsed)))))
         (:result state)))))

(defn parse-id [parsed-handler]
  (let [handler-node (z-next parsed-handler 3)]
    (z/string handler-node)))

(defn parse-params [parsed-handler]
  (let [handler-node (z-next parsed-handler 6)]
    (z/string handler-node)))

(defn analyze-params [params-str]
  (let [parsed-params (parse-re params-str)
        db (z/sexpr (z-next parsed-params 2))
        arg (z/sexpr (z-next parsed-params 3))]
    {:db db
     :arg arg}))

(defn parse-change-fnc [handler-node let?]
  (let [list-node (first-list handler-node)
        fnc (z/sexpr list-node)]
  (if let?
    (z/sexpr (second-list list-node))
    fnc)))

(defn parse-handler [handler-str file-str]
  (let [parsed (parse-re handler-str)
        current-ns (get-current-ns file-str) 
        handler-type (parse-handler-type parsed current-ns)
        id (parse-id parsed)
        params-str (parse-params parsed)
        params-parsed (analyze-params params-str)
        let? (let? parsed) 
        change-function (parse-change-fnc parsed (:let? let?))
        result {:handler-type handler-type
                :id id
                :change-fnc change-function
                :params-analyzed params-parsed}]
    (if (:let? let?)
     (merge result {:bindings? (analyze-bindings (:bindings let?))})
      result)))
