(ns src.representation)

(defn whitespaces [times]
  (loop [{:as build :keys [count]} {:string ""
                                    :count 0}]
    (if (< count times)
      (recur
       (-> build
           (update :string str " ")
           (update :count inc)))
      (:string build))))

(defn brackets [times]
  (loop [{:as build :keys [count]} {:string ""
                                    :count 0}]
    (if (< count times)
      (recur
       (-> build
           (update :string str "]")
           (update :count inc)))
      (:string build))))


(defn malli [k w]
  (cond
    (vector? k) (loop [{:as build :keys [w pairs]} {:string ""
                                                    :pairs k
                                                    :w w
                                                    :w-inside 0}]
                  (if (seq pairs)
                    (recur
                     (-> build
                         (update :string str (whitespaces w) "[" (first pairs)  (when (not= (count pairs) 1)
                                                                                  (str "\n" (whitespaces (+ w 2)) "[:map\n")) (when (= (count pairs) 1)
                                                                                                                               (brackets (dec (count pairs)))))
                         (assoc :w (+ (:w build) 4))
                         (update :pairs rest)))
                    (:string build)))
    :else (str (whitespaces w) "[" k)))

(defn build-representation [analyzed-pairs]
  (let [start "```text\n [:map\n"
        end "]\n```"]
    (if (vector? analyzed-pairs)
      (str start
           (loop [{:as build :keys [w pairs]} {:string ""
                                               :pairs analyzed-pairs
                                               :w 4}]
             (if (seq pairs)
               (recur
                (-> build
                    (update :string str
                            (malli (first (first pairs)) w)
                            " "
                            (second (first pairs))
                            "]"
                            (when (vector? (first (first pairs)))
                              (brackets (dec
                                         (count
                                          (first (first pairs))))))
                            (when (not= (count pairs) 1)
                              "\n"))
                    ;(update :count #(+ 4 %))
                    (update :pairs rest)))
               (:string build))) end)
      analyzed-pairs)))

