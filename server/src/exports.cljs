(ns src.exports
  (:require 
   [src.file-parser :as fp]
   [src.handler-parser :as hp]
   [src.analyzer :as analyzer] 
   [src.representation :refer [build-representation]]))

(defn ^:export save-ns [file-str]
  (fp/parse-ns file-str)
  (fp/parse-file-for-token file-str)
  (clj->js "Saved"))

(defn ^:export ids [file-str]
  (clj->js (fp/parse-for-ids file-str)))

(defn ^:export show-hover [file-str handler-str]
  (let [parsed-handler (hp/parse-handler
                        (fp/parse-for-handler file-str handler-str)
                        file-str)
        analyzed-hanlder (analyzer/analyze-handler
                          parsed-handler
                          file-str)
        representation (build-representation
                        analyzed-hanlder)]
  (clj->js representation)))

