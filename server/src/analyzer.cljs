(ns src.analyzer
  (:require
   [clojure.string :as str]
   [src.db :refer [get-in-db]]
   [rewrite-clj.zip :as z]
   [src.util-parser :refer [parse-re z-next parse-e get-current-ns]]
   [src.types :refer [clojure-core key-to-type]]))

(def supported-changes #{"assoc" "assoc-in" "update" "update-in" "dissoc" "->"})

(defn change [change-fnc-str]
  (let [parsed (parse-re change-fnc-str)
        change (z/string (z-next parsed 2))]
    {:change change
     :supported? (contains? supported-changes change)}))

(defn analyze-assoc [change-str]
  (let [parsed (parse-e change-str true)
        cleaned (rest (rest (first parsed)))]
    (loop [{:as state :keys [parsed]} {:parsed cleaned
                                              :result []}]

      (if (seq parsed)
        (recur
         (-> state
             (update :result conj [(first parsed) (second parsed)])
             (assoc :parsed (rest (rest parsed)))))
        (:result state)))))

(defn analyze-update [change-str]
  (let [parsed (parse-e change-str true)
        cleaned (rest (rest (first parsed)))
        pair (-> []
                 (conj (first cleaned))
                 (conj (list (second cleaned))))]
    (-> []
        (conj pair))))

(defn analyze-thread-first [change-str]
  (let [parsed (parse-e change-str true)
        db (second (first parsed))
        afnc (rest (rest (first parsed))) 
        re-arranged (map (fn [expr]
                           (let [change-fnc (first expr)
                                 without (rest expr)]
                             (cons change-fnc (cons db without)))) afnc)]
    (loop [{:as state :keys [functions]} {:functions re-arranged
                                          :result []}]
      (let [change-fnc (:change (change (str (first (first functions)))))
            str-fnc (str (first functions))]
      (if (seq functions)
        (recur
         (-> state
             (update :result concat (cond
                                      (= "assoc" change-fnc) (analyze-assoc str-fnc)
                                      (= "assoc-in" change-fnc) (analyze-assoc str-fnc)
                                      (= "update" change-fnc) (analyze-update str-fnc)
                                      (= "update-in" change-fnc) (analyze-update str-fnc)
                                      :else [[]]))
             (update :functions rest)))
        (:result state))))))

(defn analyze-dissoc [change-str]
  "")

(defn in-local? [to-analyze local-symbols]
  (contains? local-symbols to-analyze))

(defn check-obvs [v]
  (cond 
    (string? v) :string
    (true? v) :boolean
    (false? v) :boolean
    (vector? v) :vector
    (map? v) :map
    (keyword? v) :keyword 
    (int? v) :int
    (set? v) :set
    (fn? v) :fn
    :else false))

(defn check-function [fnc] 
  (let [parsed (parse-e (str fnc) false)
        fnc (first parsed)
        contains-core? (contains? clojure-core fnc)] 
    (if contains-core?
      (get-in clojure-core [fnc :ret])
      :unknown)))

(defn in-global? [v file-str]
  (let [v-parsed (if (= :list (z/tag (z/next (parse-re (str v)))))
                   (z/sexpr (z/next (z/next (parse-re (str v)))))
                   (z/sexpr (z/next (parse-re (str v)))))
        current-ns (get-current-ns file-str)
        requires (get-in-db [current-ns :requires]) 
        known-in-ns? (contains? (get-in-db [current-ns :known-token]) v-parsed)

        aliases (keys (get-in-db [current-ns :aliases]))
        aliases-check (get-in-db [current-ns :aliases (first (filter #(not (nil? %)) (map (fn [aliases]
                                                                                            (when
                                                                                             (re-find (re-pattern (str aliases "/")) (str v-parsed))
                                                                                              aliases))
                                                                                          aliases)))])
        refer-check (first (filter #(not (nil? %)) (map (fn [require]
                                                          (when
                                                           (contains? require :refer)
                                                            (when
                                                             (some #{v-parsed} (:refer require))
                                                              (:lib require))))
                                                        requires)))]

    (cond
      known-in-ns? (get-in-db [current-ns :known-token v-parsed])
      refer-check (get-in-db [refer-check :known-token v-parsed])
      aliases-check (get-in-db [aliases-check :known-token
                                (symbol
                                 (second
                                  (str/split
                                   (str v-parsed) "/")))]))))

(defn check-global [known-val]
  (cond
    (and (contains? known-val :params)
         (contains? known-val :function)) (check-function (:function known-val))
    :else (check-obvs known-val)))

(defn check-local [v local-symbols file-str]
  (cond
    (in-local? v local-symbols) (cond
                                  (nil? (v local-symbols)) :unknown
                                  (check-obvs (v local-symbols)) (check-obvs (v local-symbols))
                                  (in-global? (v local-symbols) file-str) (check-global (in-global? (v local-symbols) file-str))
                                  :else (if (seq? (v local-symbols))
                                          (check-function (v local-symbols))
                                          :unknown))
    :else false))

(defn analyze-value [v local-symbols file-str]
  (key-to-type
   (cond
     (check-obvs v) (check-obvs v) 
     (check-local v local-symbols file-str) (check-local v local-symbols file-str)
     (in-global? v file-str) (check-global (in-global? v file-str)) 
     :else (if (seq? v)
             (check-function v)
             :unknown))))

(defn analyze-key [k local-symbols file-str]
  (cond
    (and (symbol? k)
         (in-global? k file-str)) (in-global? k file-str)
    (and (in-local? k local-symbols)
         (symbol? k)) (analyze-key (k local-symbols) local-symbols file-str)
    :else k))

(defn analyze-pairs [pairs local-symbols file-str]
  (loop [{:as state :keys [parsed]} {:parsed (filterv seq pairs)
                                     :result []}]
    (if (seq parsed)
      (recur
       (-> state
           (update :result conj [(analyze-key (first (first parsed)) local-symbols file-str) (analyze-value (second (first parsed)) local-symbols file-str)])
           (assoc :parsed (rest parsed))))
      (:result state))))

(defn local-symbls [params bindings?]
  (let [arg-map (if (vector? (:arg params))
                  (loop [{:as state :keys [params]} {:params (:arg params)
                                                     :result {}}]
                    (if (seq params)
                      (recur (-> state
                                 (assoc :params (rest params))
                                 (assoc-in  [:result (first params)] nil)))
                      (:result state)))
                  {(:arg params) nil})]
    (merge bindings? arg-map {(:db params) nil})))

(defn not-supported [id]
  (str "Handler " id " is not supported by Re-Vision"))

(defn analyze-handler [{:as handler :keys [id change-fnc params-analyzed bindings?]} file]
  (let [change (change (str change-fnc))
        supported? (:supported? change)
        analyze? (when
                  supported?
                   (cond
                     (= (:change change) "assoc") analyze-assoc
                     (= (:change change) "assoc-in")  analyze-assoc
                     (= (:change change) "update") analyze-update
                     (= (:change change) "update-in") analyze-update
                     (= (:change change) "->") analyze-thread-first
                     (= (:change change) "dissoc") analyze-dissoc))
        state-pairs (when
                     supported?
                      (analyze? (str change-fnc)))
        local-symbols (when
                       supported?
                        (local-symbls params-analyzed bindings?))
        analyzed-pairs (when
                        supported?
                         (analyze-pairs state-pairs local-symbols file))]
    (if supported?
      analyzed-pairs
      (not-supported id))))