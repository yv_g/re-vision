(ns src.util-parser
  (:require
   [edamame.core :as e] 
   [rewrite-clj.zip :as z]))

(defn parse-e [str all?]
  (if all?
    (e/parse-string-all str {:all true
                             :auto-resolve name})
    (e/parse-string str {:all true
                         :auto-resolve name})))
(defn parse-re [file-str]
  (z/of-string* file-str {:track-position? false}))

(defn z-next [node depth]
  (loop [{:as loop :keys [count]} {:nodes node
                                         :count 0}]
    (if (< count depth)
      (recur
       (-> loop
           (update :nodes z/next)
           (update :count inc)))
      (:nodes loop))))

(defn get-current-ns [file-str]
  (let [parsed (z/of-string file-str)]
    (z/sexpr (z-next parsed 2))))

(defn require-as? [requires lib]
  (let [as-list (filter #(not (nil? %))
                        (map (fn [require]
                               (if
                                (= (get require :lib) lib)
                                 (when (contains? require :as)
                                   (get require :as))
                                 nil)) requires))]
    (first as-list)))
