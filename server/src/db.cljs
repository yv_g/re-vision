(ns src.db)

(def global-db (atom {:known-ns #{}}))

(defn assoc-in-db [k v]
  (swap! global-db assoc-in k v))

(defn get-in-db [k]
  (get-in @global-db k))

(defn reset []
  (reset! @global-db {}))
