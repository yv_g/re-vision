(ns src.file-parser
  (:require [src.db :refer [assoc-in-db get-in-db]]
            [src.util-parser :refer [parse-e parse-re z-next get-current-ns require-as?]]
            [edamame.core :as e]
            [rewrite-clj.zip :as z]))

(defn parse-ns [file-str]
  (let [parsed (e/parse-ns-form (parse-e file-str false))]
    (println parsed)
    (assoc-in-db [:known-ns] (conj (get-in-db [:known-ns]) (:current parsed)))
    (assoc-in-db [(:current parsed) :aliases] (:aliases parsed))
    (assoc-in-db [(:current parsed) :requires] (:requires parsed))))

(defn docstring? [sus]
  (if (= "\"" (first
               (z/string (z/right (z/right (z/right (z/down sus)))))))
    (z/sexpr (z/right (z/right (z/right (z/right (z/down sus))))))
    (z/sexpr (z/right (z/right (z/right (z/down sus)))))))

(defn parse-file-for-token [file-str]
  (let [parsed (parse-re file-str)
        init-state {:ns (get-current-ns file-str)
                    :parsed parsed
                    :known-token {}}]
    (loop [{:as state :keys [parsed]} init-state]
      (if (not (z/end? parsed))
        (recur
         (cond
           (= :list (z/tag parsed))
           (cond
             (= (z/string (z/next parsed)) "def") (-> state
                                                      (update :parsed z/next)
                                                      (assoc-in  [:known-token (z/sexpr (z-next parsed 2))] (z/sexpr (z-next parsed 3))))
             (= (z/string (z/next parsed)) "defn") (-> state
                                                       (update :parsed z/next)
                                                       (assoc-in  [:known-token (z/sexpr (z-next parsed 2))] {:params (z/sexpr (z-next parsed 3))
                                                                                                              :function (docstring? parsed)}))
             :else
             (update state :parsed z/next))

           :else
           (update state :parsed z/next)))
        (assoc-in-db [(:ns state) :known-token] (:known-token state))))))

(defn parse-for-handler [file-str handler-str]
  (let [parsed (parse-re file-str)
        init-state {:parsed parsed}]
    (loop [{:as state :keys [parsed]} init-state]
      (if (not (z/end? parsed))
        (recur
         (if (= (z/string parsed) handler-str)
           (-> state
               (assoc :handler-str (z/string (z/up parsed)))
               (update :parsed z/next))
           (update state :parsed z/next)))
        (:handler-str state)))))

(defn create-tags [import]
  (loop [{:as state :keys [import events]} {:import import
                                            :events ["reg-event-db"]
                                            :result #{}}]
    (if (seq events)
      (recur
       (-> state
           (update :result conj (if import
                                  (str import "/" (first events))
                                  (first events)))
           (update :events rest)))
      (:result state))))

(defn parse-for-ids [file-str]
  (let [parsed (parse-re file-str)
        parsed-w-ns (z/right (z/down parsed))
        current-ns (get-current-ns file-str)
        re-frame-import? (require-as? (get-in-db [current-ns :requires]) 're-frame.core)
        event-tags (create-tags re-frame-import?)]
    (loop [{:as state :keys [parsed]} {:parsed parsed-w-ns
                                           :ids []}]

      (if (not (z/end? parsed))
        (recur
         (if
          (contains? event-tags (z/string parsed))
           (-> state
               (update :ids conj (z/string (z/next parsed)))
               (update :parsed z/next))
           (update state :parsed z/next)))
        (:ids state)))))